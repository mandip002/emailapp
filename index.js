const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
require('./models/User');
require('./services/passport');



mongoose.connect(keys.mongoURI, {useNewUrlParser:true});


const server = express();


server.use(cookieSession({
    name: 'session',
    keys: [keys.cookieKey],
  
    // Cookie Options
    maxAge: 30 * 24 * 60 * 60 * 1000 // 30 days
  }))


server.use(passport.initialize());
server.use(passport.session());

require('./routes/authRoutes')(server);
require('./routes/homeRoutes')(server);


const PORT = process.env.PORT || 5000;

server.listen(PORT, (err)=>{
    if(err) {
        console.log(err)
    } else {
        console.log(`server running on ${PORT}`)
    }
})